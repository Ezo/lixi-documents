import { Component, ChangeDetectorRef } from '@angular/core';
import { DocumentType, Document } from 'doc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] 
})
export class AppComponent {
  title = 'lixi-document';
  docList: Document[] = [
    {
      id: 'abc123',
      author: 'Sylvain Cloutier',
      code: 'BV987-009',
      date: new Date(),
      description: 'Rapport d\'intervention',
      title: 'Intervention #12312',
      type: 'INTE'
    }, {
      id: 'bvc432',
      author: 'Alexandre Gagné',
      code: 'BV987-010',
      date: new Date(),
      description: 'Rapport post-intervention',
      title: 'Intervention #84937',
      type: 'PINT'
    }
  ];

  docTypeOptions: DocumentType[] = [
    {
      value: 'INTE',
      label: 'Rapport d\'intervention'
    },
    {
      value: 'PINT',
      label: 'Rapport post-intervention'
    }
  ];

  onView(doc: Document) {
    console.log('Viewing document', doc);
  }

  
  onEdit(doc: Document) {
    if (doc.id) {
      console.log('End Edit', doc);
    } else {
      console.log('End Create', doc);
    }
    // Sauvegarder au backend...
  }

  onRemove(doc: Document) {
    console.log('Removing document', doc);
  }
}
