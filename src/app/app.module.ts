import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DocModule } from 'doc';

@NgModule({
  declarations: [
    AppComponent 
  ],
  imports: [
    BrowserModule,
    DocModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
