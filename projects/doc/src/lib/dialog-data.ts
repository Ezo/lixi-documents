import { Document } from "./document";
import { DocumentType } from "./document-type";

export class DialogData {
    document: Document;
    documentTypeOptions: DocumentType[];
}