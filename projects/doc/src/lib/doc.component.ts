import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Document } from './document';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { DocEditComponent } from './doc-edit.component';
import { DialogData } from './dialog-data';
import { DocumentType } from './document-type';

@Component({
  selector: 'lixi-doc',
  templateUrl: './doc.component.html',
  styleUrls: ['./doc.component.scss']
})
export class DocComponent implements OnInit {
  @Output() view: EventEmitter<Document> = new EventEmitter<Document>();
  @Output() edit: EventEmitter<Document> = new EventEmitter<Document>();
  @Output() remove: EventEmitter<Document> = new EventEmitter<Document>();

  @Input() documents: Document[];
  @Input() documentsType: string;
  @Input() documentTypeOptions: DocumentType[];

  dataSource = new MatTableDataSource<Document>();
  displayedColumns: string[] = ['date', 'type', 'title', 'actions'];
  
  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource.data = this.documents;
  }

  addDocument() {
    const dialogData: DialogData = {
      document: new Document(),
      documentTypeOptions: this.documentTypeOptions
    };

    const dialogRef = this.dialog.open(DocEditComponent, {
      data: dialogData 
    });

    dialogRef.afterClosed().subscribe(result => this.edit.emit(result));
  }

  formatDate(date: Date): string {
    const dateDate = date.getDate();
    const dateMonth = date.getMonth() + 1;
    const dateYear = date.getFullYear();

    return `${(dateDate < 10 ? '0' : '')}${dateDate}/${(dateMonth < 10 ? '0' : '')}${dateMonth}/${dateYear}`;
  }

  handleViewBtnClick(selectedDocument: Document) {
    this.view.emit(selectedDocument);
  }

  handleEditBtnClick(selectedDocument: Document) {
    const dialogData: DialogData = {
      document: selectedDocument,
      documentTypeOptions: this.documentTypeOptions
    };

    const dialogRef = this.dialog.open(DocEditComponent, {
      data: dialogData 
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.edit.emit(result);
    });
  }

  handleRemoveBtnClick(selectedDocument: Document) {
    this.remove.emit(selectedDocument);
  }
}
