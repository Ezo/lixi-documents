import { NgModule } from '@angular/core';
import { DocComponent } from './doc.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule, MatButtonModule, MatTableModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule } from '@angular/material';
import { DocEditComponent } from './doc-edit.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DocComponent, DocEditComponent],
  entryComponents: [DocEditComponent],
  imports: [
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatButtonModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule
  ],
  exports: [DocComponent]
})
export class DocModule { }
