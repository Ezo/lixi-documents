export class Document {
    id: string;
    title: string;
    type: string;
    description: string;
    author: string;
    date: Date;
    code: string;
}