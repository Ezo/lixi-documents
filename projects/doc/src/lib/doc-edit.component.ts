import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormBuilder, Validators } from "@angular/forms";
import { Document } from "./document";
import { DialogData } from "./dialog-data";
import { DocumentType } from "./document-type";

@Component({
    selector: 'lixi-doc-edit',
    templateUrl: 'doc-edit.component.html',
    styleUrls: ['./doc-edit.component.scss']
})
export class DocEditComponent {
        documentForm = this.fb.group({
        title: [this.data.document.title, Validators.required],
        type: [this.data.document.type, Validators.required],
        description: [this.data.document.description, Validators.required],
        author: [this.data.document.author, Validators.required],
        date: [this.data.document.date, Validators.required],
        code: [this.data.document.code, Validators.required],
    });

    documentTypeOptions: DocumentType[] = this.data.documentTypeOptions;

    constructor(
        private fb: FormBuilder,
        private dialog: MatDialogRef<DocEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    onFormSubmit() {
        if (this.documentForm.valid) {
            const returnedData: Document = {
                id: this.data.document.id,
                author: this.documentForm.get('author').value,
                code: this.documentForm.get('code').value,
                date: new Date(this.documentForm.get('date').value),
                description: this.documentForm.get('description').value,
                title: this.documentForm.get('title').value,
                type: this.documentForm.get('type').value
            };

            this.dialog.close(returnedData);
        }
    }
}