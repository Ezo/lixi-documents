export class DocumentType {
    value: string;
    label: string;
}