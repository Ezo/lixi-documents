/*
 * Public API Surface of doc
 */

export * from './lib/doc.service';
export * from './lib/doc.component';
export * from './lib/doc.module';
export * from './lib/document';
export * from './lib/document-type';
